class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        list_length = len(nums)
        for x in range(list_length):
            for y in range(x+1,list_length):
                if nums[x] + nums[y] == target:
                    return [x,y]
